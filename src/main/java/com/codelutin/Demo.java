package com.codelutin;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Ctrl + shift + "+" pour déplier le code
 * Ctrl + shift + "-" pour replier le code
 * Ctrl + alt + l
 * Ctrl + shift + flèche du haut déplacer le code en haut
 * Ctrl + shift + flèche du bas déplacer le code en bas
 * Ctrl + Y supprimer une ligne
 * public final = peux pas modifier
 * Hello world!
 */
public class Demo {

    public static void main(String[] args) {

        /* EXEMPLE 1
        final double PI = 3.14d;

        int x=2, y=5, max;
        max = (x>y) ? x : y;

        System.out.println("Hello World!");
        System.out.println("Le résultat " + PI * 4.67d);
        System.out.println("Valeur de max " + max);
        */

        /*EXO 1*/

        Scanner keyboardInput = new Scanner(System.in);

        System.out.println("Entrez un nombre");
        long nombre1 = keyboardInput.nextLong();

        System.out.println("Entrez un autre nombre");
        long nombre2 = keyboardInput.nextLong();

        if (nombre1 > nombre2){
            System.out.println("Le nombre1 est le plus grand : " + nombre1);
        }else if(nombre1 == nombre2){
            System.out.println("Les deux nombres sont égaux " + nombre1 + " = " + nombre2);
        }else{
            System.out.println("Le nombre2 est le plus grand : " + nombre2);
        }

    }
}
